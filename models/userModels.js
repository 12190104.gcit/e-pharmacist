const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Please tell us your name!'],
    },
    email: {
        type: String,
        require: [true, 'please tell us your email!'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    role: {
        type: String,
        enum: ['user','sme','pharmacist','adamin'],
        default: 'user',
    },
    password: {
        type: String,
        require: ['true', 'Please provide a password!'],
        minlength: 8,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})

const User = mongoose,model('User', userSchema)
module.exports = User